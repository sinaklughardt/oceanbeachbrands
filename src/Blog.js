import Footer from "./Footer"
import { Link } from "react-router-dom"

export default function BlogSection() {
    return (
        <>
  <div class="bg-black md:py-24 sm:py-5">
    <div class="mx-auto max-w-7xl px-6 lg:px-8">
      <div class="mx-auto max-w-2xl text-center">
        <h2 class="text-4xl font-bold  text-cursive didact-gothic-regular tracking-tight text-white sm:text-4xl lg:col-span-7">Blog</h2>
        <p class="mt-2 text-lg leading-8 text-cursive didact-gothic-regular text-gray-300">Learn everything there is to know about non-alcoholic beverages</p>
      </div>
      <div class="mx-auto mt-16 grid max-w-2xl grid-cols-1 gap-x-8 gap-y-20 lg:mx-0 lg:max-w-none lg:grid-cols-3">
      <Link to="/blog/welcomezeronimo">
        <article v-for="post in posts" key="post.id" class="flex flex-col items-start justify-between">
          <div class="relative w-full">
            <img
          src={require("./pictures/welcomezeronimo.jpg")}
          alt="Welcome Zeronimo"
            class="aspect-[16/9] w-full rounded-2xl bg-gray-100 object-cover sm:aspect-[2/1] lg:aspect-[3/2]" />
            <div class="absolute inset-0 rounded-2xl ring-1 ring-inset ring-gray-900/10" />
          </div>
          <div class="max-w-xl">
            <div class="mt-8 flex items-center gap-x-4 text-xs">
              <time datetime="post.datetime" class="text-gray-100">June 11, 2024</time>
            </div>
            <div class="group relative">
              <h3 class="mt-3 text-lg font-semibold leading-6 text-gray-300 group-hover:text-gray-200">
                <a >
                  <span class="absolute inset-0" />
                  Ocean Beach Brands Welcomes ZERONIMO!
                </a>
              </h3>
              <p class="mt-5 line-clamp-3 text-sm leading-6 text-gray-200">by Joshua James</p>
            </div>

          </div>
        </article>
        </Link>
      </div>
    </div>
  </div>
  <Footer/>
        </>
    )
}
