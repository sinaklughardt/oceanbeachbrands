
import Footer from "../Footer"
import { Link } from "react-router-dom"

export default function June11 () {

    return (
        <div className="bg-black text-gray-200 didact-gothic-regula pt-5">
            <div className="text-center my-10">
            <h3 className="text-4xl font-bold">Ocean Beach Brands Welcomes ZERONIMO!</h3>
            <p className="mt-5">By Joshua James</p>
            <p>June 12, 2024</p>
            </div>
            <p className="mx-5 md:mx-10">I have great news for wine lovers everywhere! On May 16, 2024, Ocean Beach Brands partnered with Katja Bernegger and Patrick Bayer to launch ZERONIMO wines in the U.S., and we have been loving the excitement leading up to this game-changing addition to the world of non-alcoholic (NA) wines. And this is just the start - here at Ocean Beach Brands, we are dedicated to sourcing the best non-alcoholic products from around the world. Keep your eyes out for new and exciting products that are revolutionizing the beverage industry!</p>
            <img
            className="w-60 mx-auto m-5"
            src={require("../pictures/joshwithwine.jpg")}
            alt="Josh with Leonis"></img>
            <div className="mx-5 md:mx-10">
            <div className="text-2xl font-bold mb-3">
            The ZERONIMO Difference
            </div>
            <p>ZERONIMO is not your typical mass-produced non-alcoholic wine. This family-owned Austrian winery boasts 70-year-old vines to produce a 98-point wine that has been delicately dealcoholized to maintain its rich flavor profile.
            Having tasted countless NA wines over the past four years, I can confidently say that ZERONIMO is the best I've ever encountered. It's a testament to what can be achieved when tradition meets technology in the world of wine-making.
            </p>
            </div>
            <img
            className="w-1/2 mx-auto m-5"
            src={require("../pictures/patrickandkatja.jpg")}
            alt="Josh with Leonis"></img>
            <div className="mx-5 md:mx-10">
            <div className="text-2xl font-bold mb-3">
            ZERONIMO - A New Standard in NA Wines
            </div>
            <p>
            With wine sales down 15% in California,
            we're seeing a flurry of NA brands entering the scene to fill the gap.
            This surge in innovation is a boom for wine enthusiasts who are looking for alternatives without compromising on taste and quality.
            With younger generations seeking new and exciting beverage options, brands like ZERONIMO are getting people SO excited about “what’s good” in all the NA products that have come to market.
            That’s what we’re here for!
            </p>
            <p className="mt-3">
            I shared ZERONIMO with a wine consultant in Napa and he was thrilled with the taste and aromas of the wine -
            but more than that, we discussed the impact of ZERONIMO on the beverage industry.
            The dedication and passion behind innovations like ZERONIMO are advancing the NA landscape in unprecedented ways.
            It’s inspiring to see, and I can’t wait for everyone to immerse themselves in the world of high-quality NA products.
            </p>
            </div>
            <img
            className="w-60 mx-auto m-5"
            src={require("../pictures/zeronimobottles.png")}
            alt="Josh with Leonis"></img>
            <div className="mx-5 md:mx-10">
            <div className="text-2xl font-bold mb-3">
            Join the ZERONIMO Community and get on the Waiting List!
            </div>
            <p>Don't just take my word for it.
            Experience the excellence of ZERONIMO wines for yourself.
            We invite you to join our community of discerning wine lovers who appreciate the finer things in life - without the alcohol.
            </p>
            <p className="mt-3 underline">
            <Link to="http://oceanbeachbrands.myshopify.com">
            Pre-order ZERONIMO and be among the first to enjoy this revolutionary NA wine.
            </Link>
            </p>
            </div>
            <div className="mt-10 mx-5 md:mx-10">
            <div className="text-2xl font-bold mb-3">
            Cheers to New Beginnings!
            </div>
            <p>We are thrilled to share ZERONIMO with you and look forward to your feedback.
                Stay tuned for more updates and exciting news as we continue to innovate and elevate the NA wine experience.
            </p>
            <p className="mt-3">
            Thank you - and cheers,
            </p>
            <p className="mt-3">
            Joshua James
            </p>
            <p className="mt-3">
            The Ocean Beach Brands Team
            </p>
            <p className="mt-3">
            ---
            </p>
            <p className="mt-3">
            Keep an eye on our blog and social media channels for more exciting updates and stories. Together, let's celebrate the art of exceptional wine, one glass at a time. 🍷
            </p>
            </div>


            <Footer/>
        </div>
    )
}
