
import './App.css';
import LandingPage from './Landing';
import AboutUs from './AboutUs';
import NavBar from './NavBar';
import BlogSection from './Blog';
import { BrowserRouter, Routes, Route  } from 'react-router-dom';
import RedirectPage from './Redirect';
import June11 from './blogposts/june11-2024';

function App() {
  return (
    <>
    <BrowserRouter className="bg-black">
    <NavBar/>
    <Routes>
      <Route
      path="/"
      element={<LandingPage/>}>
      </Route>
      <Route
      path="/aboutus"
      element={<AboutUs/>}>
      </Route>
      <Route
      path="/zeronimo"
      element={<RedirectPage/>}>
      </Route>
      <Route
      path="/blog"
      element={<BlogSection/>}>
      </Route>
      <Route
      path="/blog/welcomezeronimo"
      element={<June11/>}>
      </Route>
    </Routes>
    </BrowserRouter>
   </>
  );
}

export default App;
