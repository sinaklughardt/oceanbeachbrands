import React from "react";

const RedirectPage = () => {
    React.useEffect(() => {
      window.location.replace('http://oceanbeachbrands.myshopify.com')
    }, [])
  }

export default RedirectPage;
